# Mod Template

Framework for creating code only Nebulous: Fleet Command mods.

Uses Node.js to automatically setup and compile mods.

# Usage

## Prerequisite

1) .NET SDK 6.0.200
    - https://dotnet.microsoft.com/en-us/download/visual-studio-sdks.
2) Node.js v16.13.2 and NPM 8.1.2
    - https://nodejs.org/en/download/
3) `node-gyp` prerequites (https://github.com/nodejs/node-gyp#on-windows)

*Note: Exact versions are not strictly required and older versions may work. These version numbers are provided for replicating known working setup.*

## Configuration

Don't forget to modify `config.json` as needed to support correct auto-setup and generation of modinfo files.

Change the `Preview.png` as needed

![](src/Preview.png){width=100px height=100px}

## Installation

1) Git clone or download this repository (https://gitlab.com/nebfltcom/mod-template/-/archive/main/mod-template-main.zip). Unzip if needed.
2) Open up a command prompt or powershell session and navigate to the folder that contains this file.
3) Run the command `npm install`.
4) Run the command `npm run setup`.

Note: After first time setup, running `npm run update` can be used instead of setup to avoid recopying the base game

## Start

Running `npm start` is a shortcut to starting the game with the mod added (still needs to be enabled).