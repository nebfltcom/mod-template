const config = require('../src/config.json');

module.exports = {
  generateCSProj: () => {
    return `<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <TargetFramework>net462</TargetFramework>
    <AssemblyName>${config.name}</AssemblyName>
    <Description>${config.description}</Description>
    <GUID>com.nebulous.mods.${config.name}</GUID>
    <Name>${config.longName}</Name>
    <Version>${config.version}</Version>
    <AllowUnsafeBlocks>true</AllowUnsafeBlocks>
    <LangVersion>9.0</LangVersion>
    <PlatformTarget>x64</PlatformTarget>
    <RestoreAdditionalProjectSources>
      https://api.nuget.org/v3/index.json;
      https://nuget.bepinex.dev/v3/index.json;
      https://nuget.samboy.dev/v3/index.json;
    </RestoreAdditionalProjectSources>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="HarmonyX" Version="2.10.1" />
    <PackageReference Include="UnityEngine.Modules" Version="2020.3.19" IncludeAssets="compile" />
  </ItemGroup>

  <ItemGroup>
    <Reference Include="Nebulous">
      <HintPath>lib/Nebulous.dll</HintPath>
    </Reference>
    <Reference Include="Mirror">
      <HintPath>lib/Mirror.dll</HintPath>
    </Reference>
  </ItemGroup>
  
  <ItemGroup Condition="'$(TargetFramework.TrimEnd(\`0123456789\`))' == 'net'">
    <PackageReference Include="Microsoft.NETFramework.ReferenceAssemblies" Version="1.0.2" PrivateAssets="all" />
  </ItemGroup>
</Project>`;
  },
  generateModInfo: () => {
    return `<?xml version="1.0"?>
<ModInfo xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ModName>${config.longName}</ModName>
  <ModDescription>${config.description}</ModDescription>
  <ModVer>${config.version}</ModVer>
  <GameVer>${config.gameVersion}</GameVer>
  <Assemblies>
    <string>${config.name}.dll</string>
  </Assemblies>
  <PreviewImage>Preview.png</PreviewImage>
</ModInfo>`;
  }
}