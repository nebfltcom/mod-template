﻿using System;
using System.Collections.Generic;
using UnityEngine;
using HarmonyLib;
using Modding;

namespace MyNebulousMod {
  public class MyNebulousMod : IModEntryPoint {
    public void PreLoad() {
      //Before assetbundles are loaded
    }

    public void PostLoad() {
      //Load harmony patches
      Harmony harmony = new Harmony("nebulous.my-neb-mod");
      harmony.PatchAll();
      //After assetbundles are loaded

      Debug.Log("MyNebulousMod loaded and ready to go!");
    }
  }
}
